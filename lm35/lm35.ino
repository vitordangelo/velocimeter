const int analogPin = 5;
float temperature = 0;
float valAnalog = 0;

void setup() {
  Serial.begin(115200);
}

void loop() {
  temp();
}

void temp() {
  valAnalog = analogRead(analogPin);
  temperature = (5 * valAnalog * 100) / 1024;
  Serial.println(temperature);
  delay(3000);
}
