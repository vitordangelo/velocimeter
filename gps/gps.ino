#include <SoftwareSerial.h>
#include "TinyGPS.h"
#include <EEPROM.h>
#include <LiquidCrystal.h>

const int LM35 = A0; // Define o pino que lera a saída do LM35
int temperatura; // Variável que armazenará a temperatura medida
float velocidadeMaxima = 0; //Declaro e inicializo a variável de velocidade máxima, deixando o valor inicial como zero e como float para aceitar valores "quebrados"

//Define os pinos que serão utilizados para ligação ao display
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
SoftwareSerial serial1(10, 11); // RX, TX / Ligado ao pino 10 e 11 arduino nano     //              Pinos do GPS (4 LIGADO PINO 9) e (5 LIGADO PINO 10

int horaAnterior = 0, minutoAnterior = 0;
int velMax = 0;
const int btnConfigVelMaxima = 1; //Configura o pino dois com o nome de btnConfigVelMaxima
int velocidadeMaximaDefinida = 0;

TinyGPS gps1;                                                                                  //     PINAGEM GPS : 1 não usa / 2 GND  / 3 não usa   /  4 RX   /   5  TX     /  6  VCC

void setup() {
  lcd.begin(16, 2);
  serial1.begin(9600);
  Serial.begin(9600);
  velMax = EEPROM.read(0);
  Serial.println("O GPS do Brincando com Ideias aguarda pelo sinal dos satelites, vai galão...");

  pinMode(btnConfigVelMaxima, INPUT);
}

void loop() {

  if (velocidadeMaximaDefinida == LOW) {
    velocidadeMaximaDefinida ++;
    Serial.println(velocidadeMaximaDefinida);
    delay(200);
  }

  bool recebido = false;
  while (serial1.available()) {
    char cIn = serial1.read();
    recebido = gps1.encode(cIn);   
  }

  if (recebido) {
    Serial.println("----------------------------------------");
     
    //Latitude e Longitude
    long latitude, longitude;
    unsigned long idadeInfo;
    gps1.get_position(&latitude, &longitude, &idadeInfo);     

    if (latitude != TinyGPS::GPS_INVALID_F_ANGLE) {
      //Serial.print("Latitude: ");
      //Serial.println(float(latitude) / 100000, 6);
    }

    if (longitude != TinyGPS::GPS_INVALID_F_ANGLE) {
      // Serial.print("Longitude: ");
      // Serial.println(float(longitude) / 100000, 6);
    }

    if (idadeInfo != TinyGPS::GPS_INVALID_AGE) {
      // Serial.print("Idade da Informacao (ms): ");
      //Serial.println(idadeInfo);
    }

    //Dia e Hora
    int ano;
    byte mes, dia, hora, minuto, segundo, centesimo;
    gps1.crack_datetime(&ano, &mes, &dia, &hora, &minuto, &segundo, &centesimo ,&idadeInfo);

    Serial.print("Data (GMT): ");
    Serial.print(dia);
    Serial.print("/");
    Serial.print(mes);
    Serial.print("/");
    Serial.println(ano);
    
    Serial.print("Horario (GMT): ");
    Serial.print(hora);
    Serial.print(":");
    Serial.print(minuto);
    Serial.print(":");
    Serial.print(segundo);
    Serial.print(":");
    Serial.println(centesimo);

    //altitude
    float altitudeGPS;
    altitudeGPS = gps1.f_altitude();

    if ((altitudeGPS != TinyGPS::GPS_INVALID_ALTITUDE) && (altitudeGPS != 1000000)) {
      //Serial.print("Altitude (cm): ");
      //Serial.println(altitudeGPS);
    }

     //velocidade
    int velocidade;
    //velocidade = gps1.speed();        //nós
    velocidade = gps1.f_speed_kmph();   //km/h
    //velocidade = gps1.f_speed_mph();  //milha/h
    //velocidade = gps1.f_speed_mps();  //milha/segundo
  
    lcd.setCursor(0, 0); //Posiciona o cursor na primeira coluna(0) e na primeira linha(0) do LCD
    temperatura = (float(analogRead(LM35))*5/(1023))/0.01;
    lcd.print("TEMP:"); //Escreve no LCD temperaturac ="
    lcd.print(temperatura);
    lcd.print("    ");

    if(hora != horaAnterior || minuto !=minutoAnterior);{
     
    horaAnterior = hora;
    minutoAnterior = minuto;
    lcd.setCursor(11, 0);
    lcd.print("     ");
    lcd.setCursor(11, 0);        

    if (int(hora) < 10) {
      lcd.print("0");
      lcd.print(hora);
    } else {
      lcd.print(hora);
    }
                               
    lcd.print(":");

    if (int(minuto) < 10) {
      lcd.print("0");
      lcd.print(minuto);
    } else {
      lcd.print(minuto); 
    }
    
  }
    /*lcd.setCursor(11, 0);
    lcd.print("     ");
    lcd.setCursor(11, 0);                                   
    lcd.print(hora-2);
    lcd.print(":");
    lcd.print(minuto);*/
  

    lcd.setCursor(0, 1);
    lcd.print("VEL:");
    lcd.print(velocidade);
    lcd.print("    ");
    
    if(velocidade > velMax) velMax = velocidade;
      lcd.setCursor(8, 1);
      lcd.print("VMAX:");
      lcd.print(velMax);
      lcd.print("   ");
      EEPROM.write(0,velMax);

      //Se o botão for pressionado incrementa o valor de velocidadeMaxima
    if (btnConfigVelMaxima == HIGH) {
      velocidadeMaxima ++;
      delay(200);
    }

 
    //sentito (em centesima de graus)
    unsigned long sentido;
    sentido = gps1.course();

    //Serial.print("Sentido (grau): ");
    //Serial.println(float(sentido) / 100, 2);

    //satelites e precisão
    unsigned short satelites;
    unsigned long precisao;
    satelites = gps1.satellites();
    precisao =  gps1.hdop();

    if (satelites != TinyGPS::GPS_INVALID_SATELLITES) {
      // Serial.print("Satelites: ");
      //Serial.println(satelites);
    }

    if (precisao != TinyGPS::GPS_INVALID_HDOP) {
      //Serial.print("Precisao (centesimos de segundo): ");
      //Serial.println(precisao);
    }

    //float distancia_entre;
    //distancia_entre = gps1.distance_between(lat1, long1, lat2, long2);

    //float sentido_para;
    //sentido_para = gps1.course_to(lat1, long1, lat2, long2);
  }
}
